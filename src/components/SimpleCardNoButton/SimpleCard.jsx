import { Card, Box, styled } from "@mui/material";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
// STYLED COMPONENTS
const CardRoot = styled(Card)({
  height: "100%",
  padding: "20px 24px"
});

const CardTitle = styled("div")(({ subtitle }) => ({
  fontSize: "1rem",
  fontWeight: "500",
  textTransform: "capitalize",
  marginBottom: !subtitle && "16px"
}));

export default function SimpleCard({ children, title, subtitle,setOpenModal }) {
  return (
    <CardRoot elevation={6}>
      <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <CardTitle subtitle={subtitle} sx={{
        fontWeight: 'bold',
      }}>{title}</CardTitle>
          </Grid>
          <Grid item>
          </Grid>
        </Grid>
      {subtitle && <Box mb={2}>{subtitle}</Box>}
      {children}
    </CardRoot>
  );
}
