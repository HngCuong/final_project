
import { useState, useRef, useEffect, useCallback } from 'react';
import { formartDate, formatNumber } from '../../utils/functions';
import { FcCallback } from 'react-icons/fc';
import { CgDetailsMore } from 'react-icons/cg';
import DataGridCustomToolbar from "../DataGridCustomToolbar/DataGridCustomToolbar";
import { Box, Typography, useTheme } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { useNavigate } from 'react-router-dom';
import { TiDelete } from 'react-icons/ti';
import EditIcon from '@mui/icons-material/Edit';
import { FormModal } from '../../components/EditFormModal';
import VisibilityIcon from '@mui/icons-material/Visibility';
export function UserData({
  handleOnClick,
  handleCallToUser,
  groupNamed,
  isLoading,
  refetch,
  setIdUser,
  setPhone,
  handleOffCallToUser
}) {
  // const { user } = useAuth();
  const [search, setSearch] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const [id, setId]  = useState("");
  const [openEditModal, setEditOpenModal] = useState(false);
  function refresh(){
    refetch()
  }
  function handleOnClick(id){
    setEditOpenModal(true);
    setId(id);
  }

  const getStatusColor = (status) => {
    return status === '1' ? 'green' : 'red';
  };
  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex:0.1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.id}
      </Typography>
      )
    },
    {
      field: "name",
      headerName: "TÊN",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.name}
      </Typography>
      )
    },
    {
      field: "dayOfBirth",
      headerName: "NGÀY SINH",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {/* {formartDate(params.row.dayOfBirth, 'short')} */}
        {params.row.dayOfBirth}
      </Typography>
      )
    },
    {
      field: "dateCreated",
      headerName: "CREATED",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {/* {formartDate(params.row.dateCreated, 'short')} */}
        {params.row.dateCreated}
      </Typography>
      )
    },
    {
      field: "gender",
      headerName: "GIỚI TÍNH",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.gender === '1' ? 'Nam' : 'Nữ'}
      </Typography>
      )
    },
    {
      field: "address",
      headerName: "ĐỊA CHỈ",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.address}
      </Typography>
      )
    },
    {
      field: "status",
      headerName: "TRẠNG THÁI",
      renderCell: (params) => (
        <Typography color={getStatusColor(params.row.status)} variant="body1" fontSize="13px">
        {params.row.status === '1' ? 'Hoạt Động' : 'Hết Hạn'}
      </Typography>
      )
    },
    {
      field: "phoneNumber",
      headerName: "ĐIỆN THOẠI",
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.phoneNumber}
      </Typography>
      )
    },
    {
      headerName: "CALL",
      field: "call",
      flex:0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width:'100vw'}}> 
        <FcCallback 
        onClick={
          () => {
          handleCallToUser(params.row.phoneNumber)
          setIdUser(params.row.id)
          setPhone(params.row.phoneNumber)
          }
        }
        />
          </Box>
      )
    },
    {
      headerName: "DETAIL",
      field: "detail",
      flex:0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color:'red', width:'100vw'}}> 
            <VisibilityIcon fontSize="small"/>
        </Box>
      )
    },
    {
      headerName: "EDIT",
      field: "edit",
      flex:0.1,
      headerAlign: 'center',
    align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width:'100vw'}}> 
            <EditIcon fontSize="small" onClick={() => handleOnClick(params.row.id)} />
        </Box>
      )
    }
    
  ];

  return (
    <>
      {openEditModal ? (
        <FormModal
          id = {id}
          refetch={refresh}
          setOpenModal={setEditOpenModal}
        />
      ) : null}

      
    <Box>
    <Box
      mt="0px"
      height="75vh"
      width='74vw'
      sx={{
        "& .MuiDataGrid-root": {
          border: "none",
        },
        "& .MuiDataGrid-cell": {
          borderBottom: "none",
        },
        "& .MuiDataGrid-columnHeaders": {
          backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
          color: "black", // Màu trắng
          borderBottom: "none",
        },
        "& .MuiDataGrid-virtualScroller": {
          backgroundColor: "#FFF", // Màu xanh dương nhạt
        },
        "& .MuiDataGrid-footerContainer": {
          backgroundColor: "#black", // Màu xanh dương
          color: "#FFF", // Màu trắng
          borderTop: "none",
        },
        "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
          color: "#2196F3 !important", // Màu đỏ
        },
        ".MuiDataGrid-toolbarContainer .MuiText": {
          color: "#2196F3 !important", /* Màu văn bản */
        },
        '& .MuiDataGrid-cell': {
          fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
        },
        '& .MuiDataGrid-columnHeader': {
          fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
        },
      }}
    >
      <DataGrid
       loading={isLoading || !groupNamed}
       rows={groupNamed || []}
       columns={columns}
       getRowId={(row) => row.id}
       components={{ Toolbar: DataGridCustomToolbar }}
       componentsProps={{
            toolbar: { searchInput, setSearchInput, setSearch },
          }}
  
/>
    </Box>
  </Box>
  </>
  );
  
}
