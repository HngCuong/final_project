import React from 'react'
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect} from 'react';
import AddIcon from '@mui/icons-material/Add';
import { AiFillFileExcel } from 'react-icons/ai';
import { Button, Typography,Box } from '@mui/material';
import { useRef, useCallback } from 'react';
import { formartDate, formatNumber } from '../../../utils/functions';
import { FcCallback } from 'react-icons/fc';
import { CgDetailsMore } from 'react-icons/cg';
import DataGridCustomToolbar from "../../../components/DataGridCustomToolbar/DataGridCustomToolbar";
import { DataGrid } from "@mui/x-data-grid";
import { useAxios } from '../../../context/AxiosContex';
import VisibilityIcon from '@mui/icons-material/Visibility';
import EditIcon from '@mui/icons-material/Edit';
import  AddCampaignAdm  from '../../../components/AdminCpns/AddStaff';
export default function StaffManager() {
  //Api link
  const { getTicketName } = useAxios();
  //Usestate
  const [customerData, setCustomerData] = useState([]);
  const [id, setId] = useState("");
  const [openEditModal, setEditOpenModal] = useState(false);
  // get customers
  const {
    data: customers,
    isLoading,
    refetch,
  } = 
  useQuery(
    {
      queryKey: ['customers'],
      queryFn: async () => await getTicketName(),
    }
  );
  function handleOnClick(id){
    console.log('Co Nhan nha');
    setEditOpenModal(true);
    setId(id);
  }
  useEffect(() => {
    if (customers) {
      setCustomerData(customers);
      console.log(customers);
    }
  }, [customers]);
  const columns = [
    {
      field: "id",
      headerName: "ID",
      flex:0.1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.id}
      </Typography>
      )
    },
    {
      field: "ticketStatusId",
      headerName: "Trạng Thái",
      flex:0.1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.ticketStatusId}
      </Typography>
      )
    },
    {
      field: "title",
      headerName: "Tiêu Đề",
      flex:0.1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
      a
      </Typography>
      )
    },
    {
      field: "callId",
      headerName: "Mã Cuộc Gọi",
      flex:0.2,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.callId}
      </Typography>
      )
    },  
    {
      field: "customerId",
      headerName: "Khách Hàng",
      flex:0.2,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.customerId}
      </Typography>
      )
    },
    {
      field: "ticketTypeId",
      headerName: "Loại",
      flex:0.2,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {/* {formartDate(params.row.dayOfBirth, 'short')} */}
        {params.row.ticketTypeId}
      </Typography>
      )
    },
    {
      field: "levelId",
      headerName: "Level",
      flex:0.1,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {/* {formartDate(params.row.dateCreated, 'short')} */}
        {params.row.levelId}
      </Typography>
      )
    }, 
    {
      field: "createdBy",
      headerName: "Ngày Tạo",
      flex:0.2,
      renderCell: (params) => (
        <Typography variant="body1" fontSize="13px">
        {params.row.createdBy}
      </Typography>
      )
    },
    {
      field: "userId",
      headerName: "Nhân Viên",
      flex:0.1,
      renderCell: (params) => (
        <Button onClick={
          () => {
            handleOnClick(params.row.id)
          }
        }>
        <Typography variant="body1" fontSize="13px" >
        {/* {params.row.createdBy} */}
      </Typography>
      </Button>
      )
    },
    {
      headerName: "DETAIL",
      field: "detail",
      flex:0.1,
      headerAlign: 'center',
      align: 'center',
      renderCell: (params) => (
        <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color:'red', width:'100vw'}}> 
            <VisibilityIcon/>
        </Box>
      )
    },
  ];
  return (
    <>
      {openEditModal ? (
        <AddCampaignAdm
          idBrand={id}
          refetch={refetch}
          setEditModal={setEditOpenModal}
        />
      ) : null}
      <Box>
    <Box
      mt="0px"
      height="75vh"
      width='74vw'
      sx={{
        "& .MuiDataGrid-root": {
          border: "none",
        },
        "& .MuiDataGrid-cell": {
          borderBottom: "none",
        },
        "& .MuiDataGrid-columnHeaders": {
          backgroundColor: "hsl(209.62,66.95%,53.73%)", // Màu xanh dương
          color: "black", // Màu trắng
          borderBottom: "none",
        },
        "& .MuiDataGrid-virtualScroller": {
          backgroundColor: "#FFF", // Màu xanh dương nhạt
        },
        "& .MuiDataGrid-footerContainer": {
          backgroundColor: "#black", // Màu xanh dương
          color: "#FFF", // Màu trắng
          borderTop: "none",
        },
        "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
          color: "#2196F3 !important", // Màu đỏ
        },
        ".MuiDataGrid-toolbarContainer .MuiText": {
          color: "#2196F3 !important", /* Màu văn bản */
        },
        '& .MuiDataGrid-cell': {
          fontSize: '12px', // Điều chỉnh kích thước của chữ trong cell
        },
        '& .MuiDataGrid-columnHeader': {
          fontSize: '13px', // Điều chỉnh kích thước của chữ trong header
        },
      }}
    >
      <DataGrid
       loading={isLoading || !customerData}
       rows={customerData || []}
       getRowId={(row) => row.id}
       columns={columns}
       components={{ Toolbar: DataGridCustomToolbar }}
/>
    </Box>
  </Box>
        
        
      
    </>
  );
}
